# README #

Production version of the site is built in `dist` folder

# How to build
You need to have node installed  https://nodejs.org/en/download/

## Prepare
```
npm install
```

## Prod build
Build files to deploy in /dist folder  
```
npm run prod
```  


## Dev local build
```
npm run start
```

## Tools
https://parceljs.org/ - for building, minifying etc

Bootstrap 5 - installed via npm
