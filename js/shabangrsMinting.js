import {abi} from './shabangrsAbi.js';
import 'bootstrap';


const ethereumButton = document.querySelector('.enableEthereumButton');
const showAccount = document.querySelector('.showAccount');

var web3;
var cost = "90000000000000000";  // Gwei
var contractAdress = "0x960bf873DA3E1F06a65905456122D6f9D9868648";  // Main
//var contractAdress = "0x689B566f34161134Ae7Bed23cFa94ecCe03bc620"; //Test
var maxTokenAmount = 10000;
var mintedTokenAmount;
var maxMintedTokenAmount = 9500;
var mintAmount = 1;
var minMintAmount = 1;
var maxMintAmount = 20;
var mintingStarted;
var network;
var defalutNetwork = "main"; //"rinkeby"
var accounts = [];
var contract;
var errors = [];
var gasfee = 600000;

const init = async () => {
	setMintAmount(1);
	try
	{
		await initWeb3();
		await initCost();
		await isMintingStarted();
		await getTotalSuply();
	}
	catch(error)
	{
		updateErrors("Unable to get Contract Data...");
	}

	initEvents();

	//await connectWalet();

	await checkNetwork();
	updateTotalPrice();

	window.ethereum.on('networkChanged', async function (network) {
		clearErrors();
		await initWeb3();
		await initCost();
		await isMintingStarted();
		await getTotalSuply();
		checkNetwork();
		updateTotalPrice();
	})


	window.ethereum.on('accountsChanged', function (_accounts) {
		accounts = _accounts;
		setWalletAdress(_accounts[0]);

	})

	//   web3.eth.getGasPrice(function(error, result) {
	// 	alert(result.toString());

	// 	var gasPriceInGwei =   web3.utils.fromWei(result, 'gwei')
	// 	gasfee = gasPriceInGwei;
	// 	//var gasPriceInGwei = web3.fromWei(result, 'gwei');
	// 	alert(gasPriceInGwei);
	//   });

};

const updateMintedTokenAmount = () => {
	if(mintedTokenAmount != null){
		$('.supply-block').html(maxTokenAmount - mintedTokenAmount);
	}
};

const updateTotalPrice = () => {
	var total = cost * mintAmount / 1000000000000000000;
	$('.total-price-block').html(total);
};


//Added second id adapt for mobile version
const setMintAmount = (value) => {
	mintAmount = value;
	$('#mint-amount, #mint-amount-adapt').html(mintAmount);
	updateTotalPrice();
};

const initEvents = () => {
	$('.buy-shabangrs-btn').on("click touchstart", () => {
		mintShabangrs(mintAmount);
	});

	//Added second id adapt for mobile version
	$('#mint-amount-plus-btn, #mint-amount-plus-btn-adapt').on("click touchstart", () => {
		if(mintAmount < maxMintAmount){
			mintAmount += 1;
			setMintAmount(mintAmount);
		}
	});

	//Added second id adapt for mobile version
	$('#mint-amount-minus-btn, #mint-amount-minus-btn-adapt').on("click touchstart", () => {
		if(mintAmount > minMintAmount){
			mintAmount -= 1;
			setMintAmount(mintAmount);
		}
	});

	//Added second id adapt for mobile version
	$('#mint-amount-max-btn, #mint-amount-max-btn-adapt').on("click touchstart", () => {
		setMintAmount(maxMintAmount);
	});

	$('.connect-walet-btn').on("click touchstart", () => {
		connectWalet();
	});

};

const initWeb3 = async () => {
	web3 = new Web3(window.web3.currentProvider);
	contract = new web3.eth.Contract(abi,contractAdress);
};

const connectWalet = async () => {
	try{
		await window.web3.currentProvider.enable();
		initWeb3();
		accounts =  await ethereum.request({ method: 'eth_requestAccounts' });
		setWalletAdress(accounts[0]);
	} catch(error){
		updateErrors("Unable to connect to Metamask");
	}
};

const checkNetwork = async () => {
	network = await web3.eth.net.getNetworkType();
	//alert(network);
	console.log("Network: "+network);
	if(network !== defalutNetwork) {
		updateErrors("You need to change MetaMask to the Main Network");
	}
	updateButtonStatus();
};

const clearErrors = async (withDisposeErrorsList) => {
	if(withDisposeErrorsList!== undefined && withDisposeErrorsList === true){
		//errors = [];
	}

	$('.errors-block').html("");
}

const updateErrors = async (text) => {
	var result = "";

	if(text !== undefined && text.length>0 && errors.includes(text) === false){
		errors.push(text);
	}

	errors.forEach((e) => {
		result += "<p>" + e + "</p>";
	});

	clearErrors(false);
	$('.errors-block').html(result);
};

const setWalletAdress = async (addres) => {
	$('.connect-walet-btn').html(addres.substring(0,6) + " ... " + addres.substring(addres.length -4));

};

const initCost = async () => {
	contract.methods.cost().call( function (err, res) {
		if (err) {
			console.log("Error at initCost: ", err);
			return;
		}
		cost = res;
	});
};

const getTotalSuply = async () => {
	contract.methods.totalSupply().call( function (err, res) {
		if (err) {
			console.log("Error at getTotalSuply: ", err);
			return;
		}
		mintedTokenAmount = res;
		updateMintedTokenAmount();
		return res;
	});
};

const updateButtonStatus = () => {

	if(mintingStarted && (network === defalutNetwork || network === 'undefined') && mintedTokenAmount < maxMintedTokenAmount){
		$('.buy-shabangrs-btn').prop('disabled', false);
		$('.mint-amount').prop('disabled', false);
		clearErrors(true);
	} else {
		$('.buy-shabangrs-btn').prop('disabled', true);
		$('.mint-amount').prop('disabled', true);
	}
};

const isMintingStarted = async () => {
	contract.methods.paused().call( function (err, res) {
		if (err) {
			console.log("Error: ",err);
			return;
		}
		mintingStarted = !res;
		updateButtonStatus();
	});
};

const mintShabangrs = async (count) => {
	await connectWalet();
	await isMintingStarted();
	if(mintingStarted && network === defalutNetwork && mintedTokenAmount < maxMintedTokenAmount){
		contract.methods.mint(accounts[0], count).send({
			from: accounts[0],
			value: count * cost,
			maxPriorityFeePerGas: null,
			maxFeePerGas: null,
			gasLimit: gasfee * count
		}, (err, res) => {
			if(res) {
				console.log("The balance is: ", res);
			} else if(err){
				console.log("An error occured", err);
			}
		});
	} else {
		if( mintedTokenAmount >= maxMintedTokenAmount) {
			console.log("Minting is stopt!");
		}
		else {
			console.log("Minting is not started!");
		}
	}
};

$(document).ready(() => {
	init();
});
