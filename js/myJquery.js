$(document).ready(function () {
   $(".owl-carousel").owlCarousel({
      loop: true,
      items: 3,
      dots: false,
      navText: [
         '<svg width="16" height="24" viewBox="0 0 16 24" fill="none" xmlns="http://www.w3.org/2000/svg">\n<path d="M12.9364 0.68592L15.2697 2.96924C15.6705 3.36144 15.6705 4.00647 15.2697 4.39867L8.2323 11.2854C7.83152 11.6776 7.83152 12.3226 8.2323 12.7148L15.2697 19.6016C15.6705 19.9938 15.6705 20.6388 15.2697 21.031L12.9349 23.3159C12.5462 23.6963 11.9247 23.6963 11.536 23.3159L1.45786 13.4535C0.657568 12.6703 0.656112 11.3827 1.45463 10.5978L11.536 0.687503C11.9244 0.305628 12.5471 0.304925 12.9364 0.68592Z" fill="white"/>\n</svg>\n',
         '<svg width="16" height="24" viewBox="0 0 16 24" fill="none" xmlns="http://www.w3.org/2000/svg">\n<path d="M3.06357 0.686043L0.730308 2.96936C0.32953 3.36156 0.32953 4.00659 0.730308 4.39879L7.76768 11.2855C8.16846 11.6777 8.16846 12.3228 7.76768 12.715L0.730271 19.6017C0.329493 19.9939 0.329493 20.639 0.730272 21.0312L3.06512 23.316C3.45382 23.6964 4.07525 23.6964 4.46395 23.316L14.5421 13.4536C15.3424 12.6704 15.3439 11.3829 14.5454 10.5979L4.46402 0.687625C4.07555 0.30575 3.4529 0.305047 3.06357 0.686043Z" fill="white"/>\n</svg>\n',
      ],

      responsive: {
         1920: {
            items: 4,
            nav: true,
         },
         1500: {
            items: 4,
            nav: true,
         },
         1245: {
            items: 3,
            nav: true,
         },
         980: {
            items: 3,
            nav: false,
         },
         750: {
            items: 2,
            nav: false,
            dots: true,
         },
         420: {
            items: 1,
            nav: false,
            loop: true,
            dots: true,
         },
         320: {
            items: 1,
            nav: false,
            loop: true,
            dots: true,
         },
      },
   });

   $("#show_team").on("click", function (e) {
      $(".hidden_team_block").toggle(300);
   });

   $(".open-slider-info").on("click", function (e) {
      $(".slider_description ").toggle(300);
   });
});
